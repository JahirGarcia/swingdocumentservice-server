/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import controllers.FileViewPaneController;
import java.io.IOException;
import javax.swing.JOptionPane;
import models.FileNode;

/**
 *
 * @author Jadpa28
 */
public class FileViewPane extends javax.swing.JPanel {
    
    private FileNode fileNode;
    private FileViewPaneController fileViewPaneController;
    
    /**
     * Creates new form FileViewPane
     * @param fileNode
     */
    public FileViewPane(FileNode fileNode) {
        initComponents();
        this.fileNode = fileNode;
        setup();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        splitPane = new javax.swing.JSplitPane();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtAFile = new javax.swing.JTextArea();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        lblLines = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        lblWords = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        lblCharacters = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblProperties = new javax.swing.JTable();

        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentShown(java.awt.event.ComponentEvent evt) {
                formComponentShown(evt);
            }
        });
        setLayout(new java.awt.BorderLayout());

        splitPane.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

        jPanel2.setLayout(new java.awt.BorderLayout());

        txtAFile.setColumns(20);
        txtAFile.setRows(5);
        txtAFile.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtAFileKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(txtAFile);

        jPanel2.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jPanel1.setLayout(new java.awt.GridBagLayout());

        jLabel1.setText("Lines");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        jPanel1.add(jLabel1, gridBagConstraints);

        lblLines.setText("0");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 6);
        jPanel1.add(lblLines, gridBagConstraints);

        jLabel3.setText("Words");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        jPanel1.add(jLabel3, gridBagConstraints);

        lblWords.setText("0");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 6);
        jPanel1.add(lblWords, gridBagConstraints);

        jLabel5.setText("Characters");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        jPanel1.add(jLabel5, gridBagConstraints);

        lblCharacters.setText("0");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 6);
        jPanel1.add(lblCharacters, gridBagConstraints);

        jPanel2.add(jPanel1, java.awt.BorderLayout.PAGE_END);

        splitPane.setLeftComponent(jPanel2);

        tblProperties.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"Name", null},
                {"Type", null},
                {"Length", null},
                {"Read", null},
                {"Write", null},
                {"Last Modified", null}
            },
            new String [] {
                "Properties", "Values"
            }
        ));
        jScrollPane2.setViewportView(tblProperties);

        splitPane.setBottomComponent(jScrollPane2);

        add(splitPane, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void txtAFileKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAFileKeyPressed
        this.fileViewPaneController = new FileViewPaneController(this.txtAFile.getText());
        this.lblLines.setText(String.valueOf(this.fileViewPaneController.getLinesCount()));
        this.lblCharacters.setText(String.valueOf(this.fileViewPaneController.getCharactersCount()));
        this.lblWords.setText(String.valueOf(this.fileViewPaneController.getWordsCount()));
    }//GEN-LAST:event_txtAFileKeyPressed

    private void formComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentShown
        splitPane.setDividerLocation(0.81);
    }//GEN-LAST:event_formComponentShown
    
    private void setup() {
        
        if(this.fileNode != null) {
            new Thread(() -> {
                try {
                    this.txtAFile.setText(this.fileNode.readText());
                    this.fileViewPaneController = new FileViewPaneController(this.txtAFile.getText());
                    this.lblLines.setText(String.valueOf(this.fileViewPaneController.getLinesCount()));
                    this.lblCharacters.setText(String.valueOf(this.fileViewPaneController.getCharactersCount()));
                    this.lblWords.setText(String.valueOf(this.fileViewPaneController.getWordsCount()));
                    this.fileViewPaneController.setFileProperties(this.tblProperties.getModel(), fileNode);
                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(this, "An error occurred while opening the file.", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }).start();
        }
        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblCharacters;
    private javax.swing.JLabel lblLines;
    private javax.swing.JLabel lblWords;
    private javax.swing.JSplitPane splitPane;
    private javax.swing.JTable tblProperties;
    private javax.swing.JTextArea txtAFile;
    // End of variables declaration//GEN-END:variables
}
