/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Edwin Maltez
 */
public class FileNode implements Serializable {
    
    private File file;

    public FileNode(File file) {
        this.file = file;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
    
    public void writeText(String text) throws IOException {
        
        FileWriter fw = new FileWriter(this.file);
        BufferedWriter bw = new BufferedWriter(fw);
        
        bw.write(text);
        
        bw.close();
        
    }
    
    public String readText() throws FileNotFoundException, IOException {
       
        FileReader fr = new FileReader(this.file);
        BufferedReader br = new BufferedReader(fr);
        
        String texto = "";
        String linea;
        
        while((linea = br.readLine()) != null) {
            texto+=linea;
        }
        
        br.close();
        
        return texto;
        
    }
    
    public Object[] getProperties() {
        return new Object[] {
            this.file.getName(),
            this.file.getName().substring(file.getName().lastIndexOf('.')),
            this.file.length()/1024,
            this.file.canRead(),
            this.file.canWrite(),
            new Date(this.file.lastModified())
        };
    }

    @Override
    public String toString() {
        return this.file.getName();
    }
    
}
