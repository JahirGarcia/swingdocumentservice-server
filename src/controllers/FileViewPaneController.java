/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.util.StringTokenizer;
import javax.swing.table.TableModel;
import models.FileNode;

/**
 *
 * @author Jadpa28
 */
public class FileViewPaneController {
    
    private String noteText;

    public FileViewPaneController(String noteText) {
        this.noteText = noteText;
    }
    
    public int getLinesCount() {
        StringTokenizer st = new StringTokenizer(this.noteText, "\n");
        return st.countTokens();
    }
    
    public int getCharactersCount() {
        return this.noteText.length();
    }
    
    public int getWordsCount() {
        StringTokenizer st = new StringTokenizer(this.noteText);
        return st.countTokens();
    }
    
    public void setFileProperties(TableModel model, FileNode fileNode) {
        int i=0;
        for(Object p : fileNode.getProperties()) {
            model.setValueAt(p, i, 1);
            i++;
        }
    }
    
}
