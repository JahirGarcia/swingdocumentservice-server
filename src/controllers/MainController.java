/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import models.FileNode;

/**
 *
 * @author Edwin Maltez
 */
public class MainController {
    
    public DefaultTreeModel getTreeModel(String path) {
        DefaultTreeModel model = null;
        DefaultMutableTreeNode root = null;
        FileNode fileNode = new FileNode(new File(path));
        
        if(!fileNode.getFile().isDirectory()) {
            root = new DefaultMutableTreeNode(fileNode.getFile().getParent());
            DefaultMutableTreeNode node = new DefaultMutableTreeNode(fileNode);
            root.add(node);
        } else {
            File[] listFiles = fileNode.getFile().listFiles();
            root = new DefaultMutableTreeNode(fileNode);
            createNodes(listFiles, root);
        }
        
        model = new DefaultTreeModel(root);
        return model;
    }
    
    private void createNodes(File[] files, DefaultMutableTreeNode root) {
        for(File f : files)
            root.add(createNode(f));
    }
    
    private DefaultMutableTreeNode createNode(File file) {
        FileNode fileNode = new FileNode(file);
        DefaultMutableTreeNode node = new DefaultMutableTreeNode(fileNode);
        if(fileNode.getFile().isDirectory()) {
            createNodes(fileNode.getFile().listFiles(), node);
        }
        return node;
    }
    
    public void loadFile(File file, String descPath) throws FileNotFoundException, IOException {
        File descFile = new File(descPath+System.getProperty("file.separator")+file.getName());
        FileInputStream fis = new FileInputStream(file);
        FileOutputStream fos = new FileOutputStream(descFile);
        byte[] buffer = new byte[1024];
        int len;
        while((len = fis.read(buffer)) > 0) {
            fos.write(buffer, 0, len);
        }
        fis.close();
        fos.close();
    }
    
}
